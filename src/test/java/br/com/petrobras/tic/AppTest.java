package br.com.petrobras.tic;

import org.junit.jupiter.api.Test;

import com.google.gson.JsonSyntaxException;

import br.com.petrobras.tic.jsonTypes.AirCompaniesType;
import br.com.petrobras.tic.jsonTypes.RootObjectType;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * Unit test for simple App.
 */
class AppTest {

    /**
     * Rigorous Test.
     */
    @Test
    void testApp() {
        assertEquals(1, 1);
    }

    @Test
    void testarLogging() {
        App.testarLogging();
    }

    @Test
    void testarExtrairColecaoCiasAereas() throws JsonSyntaxException, FileNotFoundException, IOException {
        RootObjectType jsonDecodificado = App.decodificarJSon();
        Map<Integer, AirCompaniesType> mapaCiasAereas = jsonDecodificado.getMapaCiasAereas();
        assertNotNull(mapaCiasAereas);
        Set<Integer> listaChaves = mapaCiasAereas.keySet();
        assertNotNull(listaChaves);
        assertTrue(listaChaves.size() > 0);
    }

    @Test
    void testarCarregarArquivoJson() throws JsonSyntaxException, FileNotFoundException, IOException {

        RootObjectType result = App.decodificarJSon();
        testarConteudoJSon(result);

    }

    private void testarConteudoJSon(RootObjectType result) {

        assertNotNull(result);
        assertNotNull(result.getAirResponseData());
        assertNotNull(result.getAirResponseData().getAirCompanies());

    }

    @Test
    void testarLeituraArquivoParaString() throws Exception {

        String arquivoEmString = App.lerArquivoParaString();
        assertNotNull(arquivoEmString);
        assertTrue(arquivoEmString.trim().length() > 0);

    }
}
