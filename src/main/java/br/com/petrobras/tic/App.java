package br.com.petrobras.tic;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import br.com.petrobras.tic.jsonTypes.RootObjectType;

/**
 * Hello world!
 */
public final class App {
    private static final String NOME_ARQUIVO = "Search_Roundtrip_with_FlightNumber-1678999134740.json";
    private static final Gson GSON = new GsonBuilder().create();
    private static final Logger LOGGER = LogManager.getLogger(App.class);

    private App() {
    }

    /**
     * Says hello to the world.
     * 
     * @param args The arguments of the program.
     */
    public static void main(String[] args) {

        try {
            RootObjectType jsonDecodificado = decodificarJSon();
        } catch (JsonSyntaxException | IOException e) {
            LOGGER.error(e);
        }

    }

    public static void testarLogging() {
        LOGGER.trace("Hello");
        LOGGER.debug("Hello");
        LOGGER.info("Hello");
        LOGGER.warn("Hello");
        LOGGER.error("Hello");
        LOGGER.fatal("Hello");
    }

    public static RootObjectType decodificarJSon() throws JsonSyntaxException, FileNotFoundException, IOException {

        RootObjectType jsonDecodificado = GSON.fromJson(lerArquivoParaString(), RootObjectType.class);
        LOGGER.info("json decodificiado {}", jsonDecodificado);

        return jsonDecodificado;
    }

    public static String lerArquivoParaString() throws FileNotFoundException, IOException {

        String result = IOUtils.toString(App.class.getResourceAsStream(NOME_ARQUIVO), "UTF-8");
        LOGGER.debug("Leu o arquivo para a string e o resultado foi {}", result);
        return result;
    }
}
