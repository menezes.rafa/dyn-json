package br.com.petrobras.tic.jsonTypes;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RootObjectType {

    private static final Logger LOGGER = LogManager.getLogger(RootObjectType.class);

    @SerializedName("searchCompleted")
    @Expose
    private Boolean searchCompleted;

    @SerializedName("airResponseData")
    @Expose
    private AirResponseDataType airResponseData;

    public Map<Integer, AirCompaniesType> getMapaCiasAereas() {

        Map<Integer, AirCompaniesType> result = new HashMap<Integer, AirCompaniesType>();

        Set<Entry<String, AirCompaniesType>> listaCiasAereas = airResponseData.getAirCompanies().entrySet();
        for (Entry<String, AirCompaniesType> entry : listaCiasAereas) {
            AirCompaniesType value = entry.getValue();
            result.put(value.getId(), value);
        }

        LOGGER.info("mapa de Cias aéreas por ID {}", result);

        return result;
    }

}