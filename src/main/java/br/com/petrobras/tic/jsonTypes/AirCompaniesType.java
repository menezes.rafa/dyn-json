package br.com.petrobras.tic.jsonTypes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AirCompaniesType {

    @SerializedName("id")
    @Expose
    private Integer id; // "id": 26

    @SerializedName("code")
    @Expose
    private String code;// "code": "LA"

    @SerializedName("name")
    @Expose
    private String nane; // "name": "LATAM"

}
