package br.com.petrobras.tic.jsonTypes;

import java.util.Map;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AirResponseDataType {

    @SerializedName("airCompanies")
    @Expose
    private Map<String, AirCompaniesType> airCompanies;

}
